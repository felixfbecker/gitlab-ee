export default () => ({
  isValidatingCredentials: false,
  validCredentials: false,

  selectedRegion: '',
  selectedRole: '',
  selectedKeyPair: '',
  selectedVpc: '',
  selectedSubnet: '',
  selectedSecurityGroup: '',
});
